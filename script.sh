echo ''
echo ''
echo ''
echo ''
echo 'STEP 1: get updates & upgrade'
echo ''
sudo yum -y update && sudo yum -y upgrade
echo 'ALL DONE!'
echo ''
echo '====================================================='
echo '====================================================='
echo 'STEP 2: install necessities'
echo ''
sudo yum install -y nano
sudo yum install -y net-tools
sudo yum install -y httpd
sudo yum install -y links
sudo yum install -y git
sudo yum install -y rkhunter
sudo yum install -y php
sudo systemctl restart httpd.service
echo -e "<?php\nphpinfo();\n?>"  > /var/www/html/phpinfo.php
echo 'ALL DONE!'
echo ''
echo '====================================================='
echo 'STEP 3: install Docker'
sudo yum install -y yum-utils \
device-mapper-persistent-data \
lvm2

sudo yum-config-manager \
--add-repo \
https://download.docker.com/linux/centos/docker-ce.repo

sudo yum install -y docker-ce
echo ''
echo 'Finished installing Docker, now let`s configure stuff!'
echo ''
echo '====================================================='
echo 'USER "vagrant" GROUPS BEFORE CHANGES:'
echo ''
sudo groups vagrant
echo ''
echo '====================================================='
echo 'LET`S ENABLE DOCKER ON STARTUP:'
echo ''
sudo systemctl enable docker
echo ''
echo '====================================================='
echo 'ADDING A "docker" GROUP:'
echo ''
sudo groupadd docker
echo ''
echo '====================================================='
echo 'ADDING THE USER TO THE "docker" GROUP:'
echo ''
sudo usermod -a -G docker vagrant
echo ''
echo '====================================================='
echo 'USER "vagrant" GROUPS AFTER CHANGES:'
sudo groups vagrant
echo ''
echo '====================================================='
echo 'LET`S REBOOT THE SYSTEM TO APPLY THE CHANGES!'
sudo reboot